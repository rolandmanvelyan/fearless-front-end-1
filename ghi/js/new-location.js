window.addEventListener('DOMContentLoaded', async () => {
    function addStateOption(state){
        return `
        <option selected value="${state}" name="state">${state} </option>
        `
    }
    const url = 'http://localhost:8000/api/states/';
    const selectTag = document.getElementById('state');
    const response = await fetch(url);
    if (response.ok) {
        
        const data = await response.json();
        const states = data.states;
        for(let state of states){
            const html = addStateOption(Object.values(state));
            selectTag.innerHTML += html;
            }
        }    
        //________________________________
        const formTag = document.getElementById('create-location-form');
        formTag.addEventListener('submit', async event => {
          event.preventDefault();

          const formData = new FormData(formTag);
          const json = JSON.stringify(Object.fromEntries(formData));
          console.log(json); 
          //^^^ get form
          //VVV post form
          
          const locationUrl = 'http://localhost:8000/api/locations/';
          const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
                },
            };
           const response = await fetch(locationUrl, fetchConfig);
            if (response.ok) {
                formTag.reset();
                const newLocation = await response.json();
                console.log(newLocation);
            }

        });  
});